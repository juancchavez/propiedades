package com.example.propiedades.util;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.LightingColorFilter;
import android.graphics.Paint;
import android.support.v8.renderscript.Allocation;
import android.support.v8.renderscript.Element;
import android.support.v8.renderscript.ScriptIntrinsicBlur;
import android.support.v8.renderscript.RenderScript;
import android.support.v8.renderscript.ScriptIntrinsicColorMatrix;

import com.example.propiedades.Application;
import com.example.propiedades.R;
import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiscCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.utils.StorageUtils;

import java.io.File;

/**
 * Created by JuanCarlos on 25/01/15.
 */
public class ImageUtil {

    private static ImageLoader imageLoader;
    private static DisplayImageOptions options;

    public static ImageLoader getImageLoader() {
        if (imageLoader == null) {
            File cacheDir = StorageUtils.getCacheDirectory(Application.getInstance());

            ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
                    Application.getInstance()).threadPriority(10)
                    .diskCacheExtraOptions(600, 600, null)
                    .diskCache(new UnlimitedDiscCache(cacheDir))
                    .tasksProcessingOrder(QueueProcessingType.LIFO)
                    .memoryCacheExtraOptions(600, 600).build();

            imageLoader = ImageLoader.getInstance();
            imageLoader.init(config);
        }
        return imageLoader;
    }

    public static DisplayImageOptions getOptionsImageLoader() {
        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.mipmap.ic_launcher)
                .showImageForEmptyUri(R.mipmap.ic_launcher)
                .showImageOnFail(R.mipmap.ic_launcher)
                .cacheInMemory(true)
                .cacheOnDisk(true).considerExifParams(true)
                .imageScaleType(ImageScaleType.EXACTLY).build();
        return options;
    }

    public static Bitmap blurBitmap(Bitmap bitmap) {

        //Create empty bitmap with the same size of the bitmap we want to blur.
        Bitmap outBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), bitmap.getConfig());

        //Instantiate a new Rendescript.
        RenderScript rs = RenderScript.create(Application.getInstance());

        //Create in/out Allocation with the Renderscript and the in/out bitmaps.
        Allocation allIn = Allocation.createFromBitmap(rs, bitmap, Allocation.MipmapControl.MIPMAP_NONE, Allocation.USAGE_GRAPHICS_TEXTURE);
        Allocation allOut = Allocation.createFromBitmap(rs, outBitmap);

        //Create an Intrinsinc Blur Script using Renderscript.
        ScriptIntrinsicBlur blurScript = ScriptIntrinsicBlur.create(rs, Element.U8_4(rs));

        //Set radius of the blur.
        blurScript.setRadius(25.f);

        //Perform the Renderscript.
        blurScript.setInput(allIn);
        blurScript.forEach(allOut);

        //Copy the final bitmap created by tge out Allocation to the outBitmap.
        allOut.copyTo(outBitmap);

        //Destroy the Renderscript.
        rs.destroy();

        return outBitmap;
    }

    public static Bitmap grayBitmap(Bitmap bitmap) {
        //Create empty bitmap with the same size of the bitmap we want to blur.
        Bitmap outBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);

        //Instantiate a new Rendescript.
        RenderScript rs = RenderScript.create(Application.getInstance());

        //Create an Intrinsinc Blur Script using Renderscript.
        ScriptIntrinsicColorMatrix grayScript = ScriptIntrinsicColorMatrix.create(rs, Element.U8_4(rs));

        //Create in/out Allocation with the Renderscript and the in/out bitmaps.
        Allocation allIn = Allocation.createFromBitmap(rs, bitmap);
        Allocation allOut = Allocation.createFromBitmap(rs, outBitmap);

        //Perform the Renderscript.
        grayScript.setGreyscale();
        grayScript.forEach(allIn, allOut);

        //Copy the final bitmap created by tge out Allocation to the outBitmap.
        allOut.copyTo(outBitmap);

        //Recycle the original Bitmap;
        bitmap.recycle();

        //Destroy the Renderscript.
        rs.destroy();

        return outBitmap;
    }

    public static Bitmap obscureBitmap(Bitmap bitmap) {
        Canvas c = new Canvas(bitmap);
        Paint paint = new Paint();
        ColorFilter filter = new LightingColorFilter(0xff707070, 0x00000000);
        paint.setColorFilter(filter);
        c.drawBitmap(bitmap, 0, 0, paint);

        return bitmap;
    }
}
