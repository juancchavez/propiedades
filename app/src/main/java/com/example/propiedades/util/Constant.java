package com.example.propiedades.util;

/**
 * Created by JuanCarlos on 14/03/15.
 */
public class Constant {
    public static final String TAG_MOVIE_LIST_REQUEST = "Movie List Request";
    public static final String TAG_UPCOMING_MOVIE_REQUEST = "Upcoming Movie Request";
}
