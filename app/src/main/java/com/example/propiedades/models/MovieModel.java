package com.example.propiedades.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by JuanCarlos on 14/03/15.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class MovieModel implements Parcelable{
    private String title;
    private int year;
    private String synopsis;
    private String critics_consensus;
    private PosterModel posters;

    public MovieModel() {}

    public MovieModel(Parcel parcel) {
        this.title = parcel.readString();
        this.year = parcel.readInt();
        this.synopsis = parcel.readString();
        this.critics_consensus = parcel.readString();
        this.posters = parcel.readParcelable(PosterModel.class.getClassLoader());
    }

    public PosterModel getPosters() {
        return posters;
    }

    public void setPosters(PosterModel posters) {
        this.posters = posters;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getSynopsis() {
        return synopsis;
    }

    public void setSynopsis(String synopsis) {
        this.synopsis = synopsis;
    }

    public String getCritics_consensus() {
        return critics_consensus;
    }

    public void setCritics_consensus(String critics_consensus) {
        this.critics_consensus = critics_consensus;
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public MovieModel createFromParcel(Parcel in) {
            return new MovieModel(in);
        }

        public MovieModel[] newArray(int size) {
            return new MovieModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeInt(year);
        dest.writeString(synopsis);
        dest.writeString(critics_consensus);
        dest.writeParcelable(posters, 0);
    }
}
