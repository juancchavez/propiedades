package com.example.propiedades.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by JuanCarlos on 14/03/15.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class PosterModel implements Parcelable{
    private String thumbnail;

    public PosterModel() {}

    public PosterModel(Parcel parcel) {
        this.thumbnail = parcel.readString();
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public PosterModel createFromParcel(Parcel in) {
            return new PosterModel(in);
        }

        public PosterModel[] newArray(int size) {
            return new PosterModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(thumbnail);
    }
}
