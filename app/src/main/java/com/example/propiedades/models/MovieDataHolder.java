package com.example.propiedades.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Created by JuanCarlos on 14/03/15.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class MovieDataHolder {
    @JsonProperty("movies")
    public List<MovieModel> movieList;
}
