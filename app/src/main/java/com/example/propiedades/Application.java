package com.example.propiedades;

import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.util.Log;

import com.example.propiedades.fragment.dialog.LoaderFragmentDialog;

/**
 * Created by JuanCarlos on 14/03/15.
 */
public class Application extends android.app.Application{
    private static Application singleton;
    private Handler mHandler = new Handler();
    protected LoaderFragmentDialog mLoader;

    public static Application getInstance() {
        return singleton;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        singleton = this;
    }

    public void handlerPost(Runnable runnable){
        mHandler.post(runnable);
    }

    public void showLoader(final FragmentManager fManagger){
        handlerPost(new Runnable() {
            @Override
            public void run() {
                mLoader= new LoaderFragmentDialog();

                if(fManagger!=null)
                    mLoader.show(fManagger, LoaderFragmentDialog.TAG);
            }
        });
    }

    public void hideLoader(){
        if(mLoader!=null)
            handlerPost(new Runnable() {

                @Override
                public void run() {
                    try{
                        mLoader.dismiss();
                    }catch(Exception e){
                        Log.e("HideLoader", e.getMessage());
                    }
                }
            });
    }
}
