package com.example.propiedades.parser;

import com.example.propiedades.models.MovieDataHolder;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONException;

import java.io.IOException;

/**
 * Created by JuanCarlos on 14/03/15.
 */
public class MovieListParser {

    public static MovieDataHolder parseJson(String json) throws JSONException, IOException {

        MovieDataHolder dataHolder = new ObjectMapper().readValue(json, MovieDataHolder.class);

        return dataHolder;
    }
}
