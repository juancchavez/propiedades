package com.example.propiedades.listener;

/**
 * Created by JuanCarlos on 28/02/15.
 */
public interface RequestListener {
    public void onResponse(Object response);
    public void OnError(String message);
}

