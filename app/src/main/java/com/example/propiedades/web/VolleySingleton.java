package com.example.propiedades.web;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.example.propiedades.Application;

/**
 * Created by JuanCarlos on 14/03/15.
 */
public class VolleySingleton {

    private static VolleySingleton sInstance = null;
    private RequestQueue mRequestQueue;

    private VolleySingleton() {
        mRequestQueue = Volley.newRequestQueue(Application.getInstance());
    }

    public static VolleySingleton getInstance() {
        if(sInstance == null) {
            sInstance = new VolleySingleton();
        }

        return sInstance;
    }

    public RequestQueue getRequestQueue()
    {
        return mRequestQueue;
    }
}
