package com.example.propiedades.web;

import java.net.URL;

/**
 * Created by JuanCarlos on 14/03/15.
 */
public class RottenTomatoesApi {
    private static final String ROTTEN_TOMATOES_API_KEY = "danesrffktgf7kkhthrzyf36";
    private static final String URL = "http://api.rottentomatoes.com/api/public/v1.0/lists/movies/";

    public static String getMovieRequestUrl() {
        return String.format("%sin_theaters.json?apikey=%s", URL, ROTTEN_TOMATOES_API_KEY);
    }

    public static String getUpcomingMovieRequestUrl() {
        return String.format("%supcoming.json?apikey=%s", URL, ROTTEN_TOMATOES_API_KEY);
    }
}
