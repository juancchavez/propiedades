package com.example.propiedades.web.request;

import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.propiedades.listener.RequestListener;
import com.example.propiedades.models.MovieDataHolder;
import com.example.propiedades.parser.MovieListParser;
import com.example.propiedades.util.Constant;
import com.example.propiedades.web.RottenTomatoesApi;
import com.example.propiedades.web.VolleySingleton;

import org.json.JSONException;

import java.io.IOException;

/**
 * Created by JuanCarlos on 14/03/15.
 */
public class MovieListRequest {
    private static final String TAG = "MovieListRequest";

    public static void makeRequest(String url, String tag, final RequestListener listener) {
        StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                try {
                    MovieDataHolder movieDataHolder = MovieListParser.parseJson(response);

                    listener.onResponse(movieDataHolder.movieList);

                }catch (JSONException ex) {
                    Log.e(TAG, ex.getMessage());
                    listener.OnError(TAG + ": " + ex.getMessage());

                }catch (IOException ex) {
                    Log.e(TAG, ex.getMessage());
                    listener.OnError(TAG + ": " + ex.getMessage());
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                listener.OnError(TAG + ": " + error.getMessage());
            }
        });

        request.setTag(tag);

        VolleySingleton.getInstance().getRequestQueue().add(request);
    }
}
