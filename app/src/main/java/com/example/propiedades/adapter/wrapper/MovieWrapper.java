package com.example.propiedades.adapter.wrapper;

import android.support.v7.widget.CardView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Created by JuanCarlos on 14/03/15.
 */
public class MovieWrapper {
    public LinearLayout linearLayout;
    public CardView cardMovie;
    public ImageView imgThumbnail;
    public TextView txtTitle;
    public TextView txtYear;
}
