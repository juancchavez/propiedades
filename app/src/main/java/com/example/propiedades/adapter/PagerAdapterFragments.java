package com.example.propiedades.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.TypefaceSpan;

import java.util.List;

/**
 * Created by JuanCarlos on 14/03/15.
 */
public class PagerAdapterFragments extends FragmentStatePagerAdapter {

    private List<Fragment> mFragmentList;
    private List<String> mTitleList;
    private Context mContext;

    public PagerAdapterFragments(FragmentManager fm, Context context, List<Fragment> fragmentList, List<String> titlesList) {
        super(fm);
        this.mContext = context;
        this.mFragmentList = fragmentList;
        this.mTitleList = titlesList;
    }

    @Override
    public Fragment getItem(int position) {
        return this.mFragmentList.get(position);
    }

    @Override
    public int getCount() {
        return this.mFragmentList.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mTitleList.get(position);
    }
}
