package com.example.propiedades.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.graphics.Palette;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.propiedades.R;
import com.example.propiedades.adapter.wrapper.MovieWrapper;
import com.example.propiedades.models.MovieModel;
import com.example.propiedades.util.ImageUtil;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

/**
 * Created by JuanCarlos on 14/03/15.
 */
public class UpcomingMovieAdapter extends ArrayAdapter<MovieModel> {
    private Context mContext;
    private ImageLoader imageLoader;
    private DisplayImageOptions displayImageOptions;

    public UpcomingMovieAdapter(Context context) {
        super(context, 0);
        this.mContext = context;
        this.imageLoader = ImageUtil.getImageLoader();
        this.displayImageOptions = ImageUtil.getOptionsImageLoader();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View root = convertView;
        MovieWrapper viewHolder;

        if(root == null){
            root = LayoutInflater.from(mContext).inflate(R.layout.upcoming_movies_item, parent, false);
            viewHolder = new MovieWrapper();

            viewHolder.linearLayout = (LinearLayout) root.findViewById(R.id.upcoming_info_container);
            viewHolder.txtTitle = (TextView) root.findViewById(R.id.txt_title_upcoming);
            viewHolder.txtYear = (TextView) root.findViewById(R.id.txt_year_upcoming);
            viewHolder.imgThumbnail = (ImageView) root.findViewById(R.id.img_thumb_upcoming);

            root.setTag(viewHolder);
        }else{
            viewHolder = (MovieWrapper) root.getTag();
        }

        viewHolder.txtTitle.setText(getItem(position).getTitle());
        viewHolder.txtYear.setText(String.valueOf(getItem(position).getYear()));
        this.imageLoader.displayImage(getItem(position).getPosters().getThumbnail(), viewHolder.imgThumbnail, displayImageOptions);

        return root;

    }

}
