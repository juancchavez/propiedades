package com.example.propiedades.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.graphics.Palette;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.propiedades.R;
import com.example.propiedades.adapter.wrapper.MovieWrapper;
import com.example.propiedades.models.MovieModel;
import com.example.propiedades.util.ImageUtil;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

/**
 * Created by JuanCarlos on 14/03/15.
 */
public class MoviesAdapter extends ArrayAdapter<MovieModel> {
    private Context context;
    private ImageLoader imageLoader;
    private DisplayImageOptions displayImageOptions;

    public MoviesAdapter(Context context) {
        super(context, 0);
        this.context = context;
        this.imageLoader = ImageUtil.getImageLoader();
        this.displayImageOptions = ImageUtil.getOptionsImageLoader();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View root = convertView;
        MovieWrapper viewHolder;

        if(root == null){
            root = LayoutInflater.from(context).inflate(R.layout.movie_item, parent, false);
            viewHolder = new MovieWrapper();

            viewHolder.cardMovie = (CardView) root.findViewById(R.id.card_movie);
            viewHolder.txtTitle = (TextView) root.findViewById(R.id.txt_movie_name);
            viewHolder.txtYear = (TextView) root.findViewById(R.id.txt_movie_year);
            viewHolder.imgThumbnail = (ImageView) root.findViewById(R.id.img_product_thumb);

            root.setTag(viewHolder);
        }else{
            viewHolder = (MovieWrapper) root.getTag();
        }

        viewHolder.txtTitle.setText(getItem(position).getTitle());
        viewHolder.txtYear.setText(String.valueOf(getItem(position).getYear()));

        getPaletteColor(viewHolder, position);

        return root;
    }


    public void getPaletteColor(final MovieWrapper holder, int position) {

        this.imageLoader.displayImage(getItem(position).getPosters().getThumbnail(), holder.imgThumbnail, displayImageOptions, new ImageLoadingListener() {
            @Override
            public void onLoadingStarted(String s, View view) {

            }

            @Override
            public void onLoadingFailed(String s, View view, FailReason failReason) {

            }

            @Override
            public void onLoadingComplete(String s, View view, Bitmap bitmap) {
                Palette.generateAsync(bitmap, new Palette.PaletteAsyncListener() {
                    public void onGenerated(Palette palette) {

                        if (palette != null) {

                            Palette.Swatch vibrantSwatch = palette.getVibrantSwatch();

                            if (vibrantSwatch != null) {
                                holder.cardMovie.setCardBackgroundColor(vibrantSwatch.getRgb());
                                holder.txtTitle.setTextColor(vibrantSwatch.getTitleTextColor());
                                holder.txtYear.setTextColor(vibrantSwatch.getTitleTextColor());
                            }
                        }
                    }
                });
            }

            @Override
            public void onLoadingCancelled(String s, View view) {

            }
        });

    }

}
