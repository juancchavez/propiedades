package com.example.propiedades.widget;

/**
 * Created by JuanCarlos on 14/03/15.
 */
public interface IconPagerAdapter {
    /**
     * Get icon representing the page at {@code index} in the adapter.
     */
    int getIconResId(int index);

    // From PagerAdapter
    int getCount();
}