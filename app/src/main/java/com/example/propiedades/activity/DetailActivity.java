package com.example.propiedades.activity;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.graphics.Palette;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.propiedades.R;
import com.example.propiedades.models.MovieModel;
import com.example.propiedades.util.ImageUtil;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollView;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollViewCallbacks;
import com.github.ksoichiro.android.observablescrollview.ScrollState;
import com.github.ksoichiro.android.observablescrollview.ScrollUtils;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

public class DetailActivity extends ActionBarActivity implements ObservableScrollViewCallbacks {

    private static final String PRODUCT_EXTRA_KEY_NAME = "Movie";
    private static final String KEY_IMAGE_URL = "ImageUrl";
    private LinearLayout mMovieContainer;
    private ImageView mBluredHeaderImageView;
    private ImageView mMovieThumb;
    private TextView mTxtTitle;
    private TextView mTxtDescription;
    private CardView mCardDetail;
    private Toolbar mToolbar;
    private ObservableScrollView mObservableScrollView;
    private MovieModel mModel;
    private ImageLoader mImageLoader;
    private DisplayImageOptions mDisplayImageOptions;
    private int mBaseColorDark;
    private int mBaseColor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mImageLoader = ImageUtil.getImageLoader();
        this.mDisplayImageOptions = ImageUtil.getOptionsImageLoader();
        this.mModel = getIntent().getParcelableExtra(PRODUCT_EXTRA_KEY_NAME);
        this.mBaseColorDark = getResources().getColor(R.color.primary);

        setContentView(R.layout.activity_movie_detail);
        setToolbarTitle(mModel.getTitle());
        setSupportActionBar(getToolbar());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getToolbar().setBackgroundColor(Color.TRANSPARENT);
        getmObservableScrollView().setScrollViewCallbacks(this);
        setProductText();
        setProductImages();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case android.R.id.home:
                super.onBackPressed();
        }
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private Toolbar getToolbar() {
        if(this.mToolbar == null) {
            this.mToolbar = (Toolbar) findViewById(R.id.app_bar_detail);
        }

        return mToolbar;
    }

    private ObservableScrollView getmObservableScrollView() {
        if(this.mObservableScrollView == null) {
            this.mObservableScrollView = (ObservableScrollView) findViewById(R.id.scroll);
        }

        return this.mObservableScrollView;
    }

    private void setToolbarTitle(String title) {
        getToolbar().setTitle(title);
    }

    private LinearLayout getmProductContainer() {
        if(this.mMovieContainer == null) {
            this.mMovieContainer = (LinearLayout) findViewById(R.id.movie_detail_container);
        }

        return this.mMovieContainer;
    }

    private CardView getCardProduct() {
        if(this.mCardDetail == null) {
            this.mCardDetail = (CardView) findViewById(R.id.card_movie_detail);
        }

        return this.mCardDetail;
    }

    private ImageView getmProductThumbnail() {
        if(this.mMovieThumb == null) {
            this.mMovieThumb = (ImageView) findViewById(R.id.img_movie_thumbnail);
        }

        return this.mMovieThumb;
    }

    private TextView getmTxtTitle() {
        if(this.mTxtTitle == null) {
            this.mTxtTitle = (TextView) findViewById(R.id.txt_movie_detail_title);
        }

        return this.mTxtTitle;
    }

    private TextView getmTxtDescription() {
        if (this.mTxtDescription == null) {
            this.mTxtDescription = (TextView) findViewById(R.id.txt_movie_description);
        }

        return this.mTxtDescription;
    }

    private ImageView getBluredHeaderImageView() {
        if(this.mBluredHeaderImageView == null) {
            this.mBluredHeaderImageView = (ImageView) findViewById(R.id.img_movie_blur);
        }

        return this.mBluredHeaderImageView;
    }

    private void setProductImages() {
        this.mImageLoader.displayImage(mModel.getPosters().getThumbnail(), getmProductThumbnail(),
                mDisplayImageOptions, new ImageLoadingListener() {
                    @Override
                    public void onLoadingStarted(String s, View view) {

                    }

                    @Override
                    public void onLoadingFailed(String s, View view, FailReason failReason) {

                    }

                    @Override
                    public void onLoadingComplete(String s, View view, Bitmap bitmap) {

                        getBluredHeaderImageView().setImageBitmap(ImageUtil.obscureBitmap(ImageUtil.blurBitmap(bitmap)));

                        Palette.generateAsync(bitmap, new Palette.PaletteAsyncListener() {
                            public void onGenerated(Palette palette) {

                                if (palette != null) {

                                    Palette.Swatch vibrantSwatch = palette.getVibrantSwatch();
                                    Palette.Swatch darkSwatch = palette.getDarkVibrantSwatch();

                                    if (vibrantSwatch != null) {
                                        mBaseColorDark = darkSwatch.getRgb();
                                        mBaseColor = vibrantSwatch.getRgb();
                                        getmProductContainer().setBackgroundColor(darkSwatch.getRgb());
                                        getmObservableScrollView().setBackgroundColor(darkSwatch.getRgb());
                                        getCardProduct().setCardBackgroundColor(vibrantSwatch.getRgb());
                                        getmTxtTitle().setTextColor(vibrantSwatch.getTitleTextColor());
                                        getmTxtDescription().setTextColor(vibrantSwatch.getBodyTextColor());

                                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                            getWindow().setStatusBarColor(mBaseColor);
                                        }
                                    }
                                }
                            }
                        });
                    }

                    @Override
                    public void onLoadingCancelled(String s, View view) {

                    }
                });
    }


    private void setProductText() {
        getmTxtTitle().setText(mModel.getTitle());
        getmTxtDescription().setText(mModel.getSynopsis());
    }

    @Override
    public void onScrollChanged(int scrollY, boolean firstScroll, boolean dragging) {
        float alpha = 1 - (float) Math.max(0, (getBluredHeaderImageView().getHeight() - getToolbar().getHeight()) - scrollY) / (getBluredHeaderImageView().getHeight() - getToolbar().getHeight());
        getToolbar().setBackgroundColor(ScrollUtils.getColorWithAlpha(alpha, mBaseColorDark));
        getmProductThumbnail().setTranslationY(scrollY / 2);
    }

    @Override
    public void onDownMotionEvent() {

    }

    @Override
    public void onUpOrCancelMotionEvent(ScrollState scrollState) {

    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        onScrollChanged(getmObservableScrollView().getCurrentScrollY(), false, false);
    }
}
