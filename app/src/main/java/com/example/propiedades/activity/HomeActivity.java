package com.example.propiedades.activity;

import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.example.propiedades.R;
import com.example.propiedades.adapter.PagerAdapterFragments;
import com.example.propiedades.fragment.MoviesFragment;
import com.example.propiedades.fragment.UpcomingMovieFragment;
import com.example.propiedades.widget.TabPageIndicator;

import java.util.ArrayList;
import java.util.List;

public class HomeActivity extends ActionBarActivity {

    private Toolbar mToolbar;
    private TabPageIndicator mTabPageIndicator;
    private List<Fragment> mFragmentList;
    private List<String> mTitlesList;
    private PagerAdapterFragments mPagerAdapter;
    private MoviesFragment mMoviesFragment;
    private UpcomingMovieFragment mUpcomingMoviesFragment;
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        setSupportActionBar(getToolbar());
        getViewPager().setAdapter(getPagerAdapter());
        getTabIndicator().setViewPager(getViewPager());
    }

    private Toolbar getToolbar() {
        if(this.mToolbar == null) {
            this.mToolbar = (Toolbar) findViewById(R.id.app_bar);
        }

        return this.mToolbar;
    }

    private ViewPager getViewPager() {
        if(this.mViewPager == null) {
            this.mViewPager = (ViewPager) findViewById(R.id.view_pager);
        }

        return this.mViewPager;
    }

    private TabPageIndicator getTabIndicator() {
        if(this.mTabPageIndicator == null) {
            this.mTabPageIndicator = (TabPageIndicator) findViewById(R.id.tab_page_indicator);
        }

        return this.mTabPageIndicator;
    }

    private List<String> getTitlesList() {
        if(this.mTitlesList == null) {
            this.mTitlesList = new ArrayList<String>();
            this.mTitlesList.add(getResources().getString(R.string.tab_movies));
            this.mTitlesList.add(getResources().getString(R.string.tab_upcoming));
        }

        return this.mTitlesList;
    }
    private PagerAdapterFragments getPagerAdapter(){
        if(this.mPagerAdapter == null){
            this.mPagerAdapter = new PagerAdapterFragments(getSupportFragmentManager(), this, getFragmentList(), getTitlesList());
        }

        return this.mPagerAdapter;
    }

    private List<Fragment> getFragmentList(){
        if(this.mFragmentList == null){
            this.mFragmentList = new ArrayList<Fragment>();
            mFragmentList.add(getMoviesFragment());
            mFragmentList.add(getUpcomingMoviesFragment());
        }

        return mFragmentList;
    }

    private MoviesFragment getMoviesFragment(){
        if(this.mMoviesFragment == null){
            this.mMoviesFragment = new MoviesFragment();
        }

        return this.mMoviesFragment;
    }

    private UpcomingMovieFragment getUpcomingMoviesFragment(){
        if(this.mUpcomingMoviesFragment == null){
            this.mUpcomingMoviesFragment = new UpcomingMovieFragment();
        }

        return this.mUpcomingMoviesFragment;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
