package com.example.propiedades.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import com.example.propiedades.R;
import com.example.propiedades.activity.DetailActivity;
import com.example.propiedades.adapter.MoviesAdapter;
import com.example.propiedades.listener.RequestListener;
import com.example.propiedades.models.MovieModel;
import com.example.propiedades.util.Constant;
import com.example.propiedades.web.RottenTomatoesApi;
import com.example.propiedades.web.VolleySingleton;
import com.example.propiedades.web.request.MovieListRequest;

import java.util.List;

/**
 * Created by JuanCarlos on 14/03/15.
 */
public class MoviesFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener, AdapterView.OnItemClickListener{
    private static final String TAG = "MoviesFragment";
    private static final String MOVIE_EXTRA_KEY = "Movie";
    private View rootView;
    private MoviesAdapter mAdapter;
    private GridView mGridView;
    private SwipeRefreshLayout mSwipeLayout;
    private List<MovieModel> mMovieList;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_movies, container, false);
        getGridView().setAdapter(getmAdapter());
        getmSwipeLayout().setOnRefreshListener(this);
        getmSwipeLayout().setColorSchemeResources(R.color.primary);
        getGridView().setOnItemClickListener(this);
        makeRequest();

        return rootView;
    }

    private SwipeRefreshLayout getmSwipeLayout() {
        if(this.mSwipeLayout == null) {
            this.mSwipeLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_container_movies);
        }

        return mSwipeLayout;
    }

    private GridView getGridView(){
        if(this.mGridView == null){
            this.mGridView = (GridView) rootView.findViewById(R.id.grid_movies);
        }

        return this.mGridView;
    }

    private final MoviesAdapter getmAdapter(){
        if(this.mAdapter == null){
            this.mAdapter = new MoviesAdapter(getActivity());
        }

        return mAdapter;
    }

    private void makeRequest() {
        getmSwipeLayout().post(new Runnable() {
            @Override public void run() {
                getmSwipeLayout().setRefreshing(true);
            }
        });

        MovieListRequest.makeRequest(RottenTomatoesApi.getMovieRequestUrl(), Constant.TAG_MOVIE_LIST_REQUEST, new RequestListener() {
            @Override
            public void onResponse(Object response) {
                mMovieList = (List<MovieModel>) response;
                fillAdapter();
                getmSwipeLayout().setRefreshing(false);
            }

            @Override
            public void OnError(String message) {
                getmSwipeLayout().setRefreshing(false);
            }
        });
    }

    private void fillAdapter() {
        getmAdapter().clear();
        getmAdapter().addAll(mMovieList);
        getmAdapter().notifyDataSetChanged();
    }

    @Override
    public void onStop() {
        super.onStop();
        VolleySingleton.getInstance().getRequestQueue().cancelAll(Constant.TAG_MOVIE_LIST_REQUEST);
    }

    @Override
    public void onRefresh() {
        makeRequest();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Intent intent = new Intent(getActivity(), DetailActivity.class);
        intent.putExtra(MOVIE_EXTRA_KEY, (MovieModel)parent.getAdapter().getItem(position));

        String transitionName = getString(R.string.transition_movie_poster);
        ActivityOptionsCompat options =
                ActivityOptionsCompat.makeSceneTransitionAnimation(getActivity(),
                        view,   // The view which starts the transition
                        transitionName    // The transitionName of the view we’re transitioning to
                );

        ActivityCompat.startActivity(getActivity(), intent, options.toBundle());
    }

}
