package com.example.propiedades.fragment.dialog;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.propiedades.R;

/**
 * Created by JuanCarlos on 14/03/15.
 */
public class LoaderFragmentDialog extends DialogFragment {
    public static String TAG = "LoaderFragmentDialog";

    private View mRootView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_TITLE, R.style.Theme_Dialog_Transparent);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_dialog_loader, container, false);
        return mRootView;
    }

    public void closeDialog() {
        getDialog().dismiss();
    }
}
